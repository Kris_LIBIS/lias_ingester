# encoding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'lias/ingester/version'

Gem::Specification.new do |gem|
  gem.name = 'LIAS_Ingester'
  gem.version = ::LIAS::Ingester::VERSION
  gem.date = Date.today.to_s

  gem.summary = %q{LIAS Ingester.}
  gem.description = %q{A workflow implementation for ingesting into LIAS.}

  gem.authors = ['Kris Dekeyser']
  gem.email = 'kris.dekeyser@libis.be'
  gem.homepage = 'https://bitbucket.org/Kris_LIBIS/lias_ingester'
  gem.license = 'MIT'

  gem.files = `git ls-files -z`.split("\0")
  gem.executables = gem.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  gem.test_files = gem.files.grep(%r{^(test|spec|features)/})

  gem.require_paths = ['lib']

  gem.add_runtime_dependency 'LIBIS_Workflow_Mongoid', ::LIAS::Ingester::VERSION
  gem.add_runtime_dependency 'chronic_duration'

  gem.add_development_dependency 'bundler', '~> 1.6'
  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'rspec'
  gem.add_development_dependency 'coveralls'

end
