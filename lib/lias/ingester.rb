# encoding: utf-8
require 'LIBIS_Workflow_Mongoid'

module LIAS
  module Ingester

    autoload :Config, 'lias/ingester/config'

    def self.configure
      yield Config.instance
    end
  end
end