# encoding: utf-8

require 'chronic_duration'

module LIAS
  module Ingester
    module Tools
      module CallTimer
        def time
          start_time
          yield if block_given?
          ChronicDuration.output((Time.now - start_time).round(3), :format => :long)
        end
      end
    end
  end
end

